package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    String result;
    String compMove;
    Random random = new Random();
    
    public void run() {
        
        System.out.println("Let's play round " + roundCounter);

        while (true) {

            String userMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if (!userMove.equals("rock") && !userMove.equals("paper") && !userMove.equals("scissors")) {
                System.out.println("I do not understand " + userMove + ". Could you try again?");
                continue;
            }

            String compMove = rpsChoices.get(random.nextInt(rpsChoices.size()));

            if (userMove.equals(compMove)) {
                result = "It's a tie"; 
                roundCounter++;
            } else if (UserWins(userMove, compMove)) {
                result = "Human wins";
                humanScore++;
                roundCounter++;
            } else {
                result = "Computer wins";
                computerScore++;
                roundCounter++;
            }

            System.out.println("Human chose " + userMove + ", computer chose " + compMove + ". " + result);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String playOn = readInput("Do you wish to continue playing? (y/n)?");

            if (playOn.equals("y")) {
                continue;
            } else if (playOn.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            } else {
                System.out.println("I do not understand " + playOn + ". Could you try again?");
            }
        }
    }

    public boolean UserWins (String userMove, String compMove) {
        if (userMove.equals(rpsChoices.get(0))) {
            return compMove.equals(rpsChoices.get(1));
        } else if (userMove.equals(rpsChoices.get(1))) {
            return compMove.equals(rpsChoices.get(2));
        } else return compMove.equals(rpsChoices.get(0));
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}