package INF101.lab1.INF100labs;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
    }
        // [21, 22, 23]
        // [31, 32, 33]

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }
        // [11, 12, 13]
        // [31, 32, 33]


        // Oppgave 2
        ArrayList<ArrayList<Integer>> grid4 = new ArrayList<>();
        grid4.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid4.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid4.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        boolean equalSums1 = allRowsAndColsAreEqualSum(grid4);
        System.out.println(equalSums1); // false


        ArrayList<ArrayList<Integer>> grid5 = new ArrayList<>();
        grid5.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid5.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid5.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grid5);
        System.out.println(equalSums2); // false

        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums3); // true

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

        ArrayList<Integer> rows = new ArrayList<>();
        ArrayList<Integer> cols = new ArrayList<>();

        for (int i = 0 ; i < grid.size() ; i++) {
            int sum = 0;
            for (int j = 0 ; j < (grid.get(i)).size() ; j++) {
                sum = sum + grid.get(i).get(j);
            }
            rows.add(sum);
        }
        for (int j = 0 ; j < (grid.get(0)).size() ; j++) {
            int sum = 0;
            for (int i = 0 ; i < grid.size() ; i++) {
                sum = sum + grid.get(i).get(j);
            }
            cols.add(sum);
        }
        int rowCheck = rows.get(0);
        int colCheck = cols.get(0);

        for (int i = 0 ; i < rows.size() ; i++) {
            if (rows.get(i) != rowCheck) {
                return false;
            }
        }
        for (int i = 0 ; i < cols.size() ; i++) {
            if (cols.get(i) != colCheck) {
                return false;
            }
        }
        return true;
    }
}