package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        // Oppgave 1
        List<Integer> list3 = Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3);
        ArrayList<Integer> arrayList3 = new ArrayList<>(list3);
        ArrayList<Integer> removedList3 = removeThrees(arrayList3);
        System.out.println(removedList3); // [1, 2, 4]

        List<Integer> list4 = Arrays.asList(3, 3);
        ArrayList<Integer> arrayList4 = new ArrayList<>(list4);
        ArrayList<Integer> removedList4 = removeThrees(arrayList4);
        System.out.println(removedList4); // []


        // Oppgave 2
        List<Integer> list1 = Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2);
        ArrayList<Integer> arrayList1 = new ArrayList<>(list1);
        List<Integer> removedList1 = uniqueValues(arrayList1);
        System.out.println(removedList1); // [1, 2, 3]

        List<Integer> list2 = Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 5);
        ArrayList<Integer> arrayList2 = new ArrayList<>(list2);
        ArrayList<Integer> removedList2 = (ArrayList<Integer>) uniqueValues(arrayList2);
        System.out.println(removedList2); // [4, 5]



        // Oppgave 3
        ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b1 = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a1, b1);
        System.out.println(a1); // [5, 4, 0]

        ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b2 = new ArrayList<>(Arrays.asList(47, 21, -30));
        addList(a2, b2);
        System.out.println(a2); // [48, 23, -27]

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for (int x: list) {
            if (x != 3) {
                newList.add(x);
            }
        } return newList;
    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (!uniqueList.contains(list.get(i))) {
                    uniqueList.add(list.get(i));
                }
            }
        return uniqueList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }
}